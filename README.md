# Go-Docker
Learning about containerisation and Go

## How to run 
`go run main.go run {command} {args} --image {image fs path}`

## What happens
- invoke the same program but inside a new namespace
- set hostname to allow us to distinguish between being in the container and not
- chroot to change the root directory for the process
- mount the proc directory, the pseudo-filesystem the kernel uses to provide status information about the status of the system. Needed for commands like `ps`, etc.
- create a new control group, limiting the number of possible processes in the container to 10
