package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"syscall"
)

func main(){
	switch os.Args[1] {
	case "run":
		run()
	case "child":
		child()
	default:
		panic("Invalid command")
	}
}

func run(){
	fmt.Printf("Running %v as %d\n", os.Args[2:], os.Getpid())

	cmd := exec.Command("/proc/self/exe", append([]string{"child"}, os.Args[2:]...)...)
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.SysProcAttr = &syscall.SysProcAttr{
		Cloneflags: syscall.CLONE_NEWUTS | syscall.CLONE_NEWPID | syscall.CLONE_NEWNS,
		Unshareflags: syscall.CLONE_NEWNS,
	}

	if err := cmd.Run(); err != nil {
		fmt.Printf("Error running cmd : %s\n", err.Error())
	}

}

func child(){
	fmt.Printf("Running %v as %d\n", os.Args[2:], os.Getpid())
	imageDir := flag.String("fs", "ubuntu-fs", "The relative path to the filesystem for the container to use")
	workingDir, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	fmt.Printf("Using %s as the container filesystem\n", filepath.Join(workingDir, *imageDir))

	createCgroup("newCgroup", "10")

	if err := syscall.Sethostname([]byte("container")); err != nil {
		fmt.Printf("Error setting the host name : %s\n", err.Error())
	}
	if err := syscall.Chroot(*imageDir); err != nil {
		fmt.Printf("Error running chroot : %s\n", err.Error())
	}
	if err := syscall.Chdir("/"); err != nil {
		fmt.Printf("Error running chdir : %s\n", err.Error())
	}
	if err := syscall.Mount("proc", "proc", "proc", 0, ""); err != nil {
		fmt.Printf("Error mounting proc : %s\n", err.Error())
	}

	cmd := exec.Command(os.Args[2], os.Args[3:]...)
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	if err := cmd.Run(); err != nil {
		fmt.Printf("Error mounting the command : %s\n", err.Error())
	}
}

func createCgroup(groupName string, numProcesses string) {
	cgroups := "/sys/fs/cgroup"
	pids := filepath.Join(cgroups, "pids")
	if err := os.Mkdir(filepath.Join(pids, groupName), 0755); err != nil && !os.IsExist(err) {
		panic(err)
	}
	must(ioutil.WriteFile(filepath.Join(pids, fmt.Sprintf("%s/pids.max", groupName)), []byte(numProcesses), 0700))
	must(ioutil.WriteFile(filepath.Join(pids, fmt.Sprintf("%s/notify_on_release", groupName)), []byte("1"), 0700))
	must(ioutil.WriteFile(filepath.Join(pids, fmt.Sprintf("%s/cgroup.procs", groupName)), []byte(strconv.Itoa(os.Getpid())), 0700))
}

func must(err error){
	if err != nil {
		panic(err)
	}
}